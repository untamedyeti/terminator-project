# Terminator Project

This project is to make a terminator. Inspiration: (https://www.hackster.io/314reactor/the-raspbinator-b03485)
<br>References: 
- Install Open CV: https://www.pyimagesearch.com/2018/09/26/install-opencv-4-on-your-raspberry-pi/ 
- List of Cascades https://github.com/opencv/opencv/tree/master/data/haarcascades
